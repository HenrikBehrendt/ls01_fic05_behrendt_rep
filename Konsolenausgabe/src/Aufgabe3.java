
public class Aufgabe3 {
	
	public static void main(String[] args) {
		
	String f= "Fahrenheit";
	String c= "Celsius";
		
	int i=20;
	double d=28.89;	
	
	
		System.out.printf("%-12s|%10s\n", f, c);
		System.out.println("------------------------");
	
		System.out.printf("%-12d|%10.2f\n", -i , -d);
		System.out.printf("%-12d|%10.2f\n", -10 , -23.33);
		System.out.printf("%+-12d|%10.2f\n", 0 , -17.78);
		System.out.printf("%+-12d|%10.2f\n", 20 , -6.67);
		System.out.printf("%+-12d|%10.2f\n", 30 , -1.11);
		
	
		
		}
}
