
public class Ausgabe {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.out.println("Er sagte: Heute ist ein \"wunderschöner\" Tag!");
		System.out.print("Er sagte: Heute ist ein \\wunderschöner\\ Tag!");
		System.out.println("Er sagte: Heute ist ein \'wunderschöner\' Tag!");
		
		int alter = 56;
		String name = "Willi";
		
		System.out.println("Ich heiße " + name + " und bin " + alter +" Jahre alt");
		
		String s = "Java-Programm";
		System.out.printf( " \n|%20s| ", s  );	
		
	}

}
